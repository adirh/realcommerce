﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Exceptions;
using Entities.WeatherModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Realcommerce‏.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private IConfiguration _config;
        private readonly IRepositoryWrapper _repository;
        private readonly IMapper _mapper;

        public WeatherController(IConfiguration config, IRepositoryWrapper repository, IMapper mapper)
        {
            _config = config;
            this._repository = repository;
            this._mapper = mapper;
        }

        /// <summary>
        /// This method get current weather from weather API, first checking if already exist in db.
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCurrentWeather([FromQuery] string cityCode)
        {
            if (String.IsNullOrEmpty(cityCode)) throw new ArgumentNullException();
            try
            {
                long cCOde;
                long.TryParse(cityCode, out cCOde);
                if(!long.TryParse(cityCode, out cCOde))
                {
                    return BadRequest("Failed to parse cityCode param.");
                }
                // Try get current weather from db.
                CityTemprature cityTemprature = await _repository.CityTempratureRepository.GetAsync(cCOde);
                if (cityTemprature != null)
                {
                    WatherDTO s = _mapper.Map<WatherDTO>(cityTemprature);
                    return Ok(s);
                }

                // Get current weather..
                IEnumerable<GetCurrentWeather> result = await _repository.WheatherRepository.GetCurrentWeather(cityCode);

                GetCurrentWeather currentWeather = result.FirstOrDefault();
                if (currentWeather == null) return NotFound("City not found");

                // Mapping to dto.
                WatherDTO weather = _mapper.Map<WatherDTO>(currentWeather);

                // Insert to db.
                _repository.CityTempratureRepository.Insert(new CityTemprature(cCOde, weather.CelsiusTempreture, weather.WeatherText));
                await _repository.SaveAsync();

                // Return result.
                return Ok(weather);
            }
            catch (Exception ex)
            {
                // Log the error.
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This method return cities that value param satisfied.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCitiesAutoComplete([FromQuery] string value)
        {
            if (String.IsNullOrEmpty(value)) throw new ArgumentNullException();
            try
            {
                // Get cities.
                IEnumerable<APICItyModel> citiesCache = await _repository.WheatherRepository.GetCitiesAutoCompleteAsync(value);

                // Mapping to dto.
                IEnumerable<APICItyDTO> citiesDtos = _mapper.Map<IEnumerable<APICItyDTO>>(citiesCache);

                // Return result.
                return Ok(citiesDtos);
            }
            catch (WeatherAPIUnauthorizedException ex1)
            {
                throw ex1;
            }
            catch (Exception ex)
            {
                // Log the error.
                return BadRequest(ex.Message);
            }
        }
    }
}