﻿using Contracts;
using Entities.WeatherModels;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Repository
{
    public class CityTempratureRepository : RepositoryBase<CityTemprature>, ICityTempratureRepository
    {
        public CityTempratureRepository(RepositoryContext repositoryContext)
        : base(repositoryContext)
        {
        }

        public async Task<CityTemprature> GetAsync(long cityCode)
        {
            return await FindByCondition(x => x.CityCode == cityCode).AsTracking()
                .FirstOrDefaultAsync();
        }

        public void Insert(CityTemprature cityTemprature)
        {
            Create(cityTemprature);
        }
    }
}
