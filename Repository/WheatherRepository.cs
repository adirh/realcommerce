﻿using Contracts;
using Entities;
using Entities.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Repository
{
    public class WheatherRepository : IWheatherRepository
    {
        public async Task<IEnumerable<GetCurrentWeather>> GetCurrentWeather(string cityCode)
        {
            if (cityCode.Equals("215854"))
            {
                var desrializedObject = JsonConvert.DeserializeObject<IEnumerable<GetCurrentWeather>>(Consts.WEATHER_CACHE_VALUE);
                return desrializedObject;
            }
            using (var httpClient = new HttpClient())
            {
                string APIURL = $"http://dataservice.accuweather.com/currentconditions/v1/{cityCode}?apikey={Consts.WEATHERAPI_API_KEY}&language=en-us&details=false";
                var response = await httpClient.GetAsync(APIURL);
                string result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<IEnumerable<GetCurrentWeather>>(result);
            }
        }

        public async Task<IEnumerable<APICItyModel>> GetCitiesAutoCompleteAsync(string value)
        {
            if (value.Trim().ToLower() == "tel")
            {
                return JsonConvert.DeserializeObject<IEnumerable<APICItyModel>>(Consts.CITY_CACHE_TEL_VALUE);
            }

            using (var httpClient = new HttpClient())
            {
                string APIURL = $"http://dataservice.accuweather.com/locations/v1/cities/autocomplete?apikey={Consts.WEATHERAPI_API_KEY}&q={value}&language=en-us";
                var response = await httpClient.GetAsync(APIURL);
                string result = await response.Content.ReadAsStringAsync();


                if (result.ToLower().Contains("unauthorized"))
                {
                    throw new WeatherAPIUnauthorizedException();
                }

                return JsonConvert.DeserializeObject<APICItyModel[]>(result);
            }
        }
    }
}
