﻿
namespace Entities.WeatherModels
{
    public partial class Favorites
    {
        public long CityCode { get; set; }
        public string LocalizedName { get; set; }

        private Favorites()
        {

        }

        public Favorites(long cityCode, string localizedName)
        {
            CityCode = cityCode;
            LocalizedName = localizedName;
        }
    }
}
