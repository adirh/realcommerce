﻿using System.Threading.Tasks;

namespace Contracts
{
    public interface IRepositoryWrapper 
    { 
        IWheatherRepository WheatherRepository { get; }
        ICityTempratureRepository CityTempratureRepository { get; }
        Task SaveAsync();
        void Save();
    }
}
