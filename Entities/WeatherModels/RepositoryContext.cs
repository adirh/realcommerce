﻿using Microsoft.EntityFrameworkCore;

namespace Entities.WeatherModels
{
    public partial class RepositoryContext : DbContext
    {
        public RepositoryContext()
        {
        }

        public RepositoryContext(DbContextOptions<RepositoryContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CityTemprature> CityTemprature { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CityTemprature>(entity =>
            {
                entity.HasKey(e => e.CityCode)
                    .HasName("PK__CityTemp__B488218DF64D4035");

                entity.Property(e => e.CityCode).ValueGeneratedNever();

                entity.Property(e => e.CelsiusTempreture).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.WeatherText).HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
