using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Realcommerce
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilderDebug(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilderDebug(string[] args) =>
        Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
            webBuilder.UseStartup<Startup>();
            webBuilder.UseUrls("http://localhost:4200");
        });
    }
}
