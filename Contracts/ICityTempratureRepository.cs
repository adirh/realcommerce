﻿using Entities.WeatherModels;
using System.Threading.Tasks;

namespace Contracts
{
    public interface ICityTempratureRepository
    {
        Task<CityTemprature> GetAsync(long cityCode);
        void Insert(CityTemprature cityTemprature);
    }
}
