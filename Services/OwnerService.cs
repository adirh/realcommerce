﻿using AutoMapper;
using Contracts;
using Contracts.Services;
using Entities.DataTransferObjects;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class OwnerService : IOwnerService
    {
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;
        private readonly IRepositoryWrapper _repository;

		public OwnerService(ILoggerManager logger, IMapper mapper, IRepositoryWrapper repository)
		{
            this._logger = logger;
            this._mapper = mapper;
            this._repository = repository;
		}
        public async Task<IEnumerable<OwnerDto>> GetAllOwnersAsync()
        {
            try
            {
                var owners = await _repository.Owner.GetAllOwnersAsync();
                _logger.LogInfo($"Returned all owners from database.");

                var ownersResult = _mapper.Map<IEnumerable<OwnerDto>>(owners);
                return ownersResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return "Internal server error";
            }
        }
    }
}
