﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IWheatherRepository
    {
        Task<IEnumerable<APICItyModel>> GetCitiesAutoCompleteAsync(string value);
        Task<IEnumerable<GetCurrentWeather>> GetCurrentWeather(string cityCode);
    }
}
