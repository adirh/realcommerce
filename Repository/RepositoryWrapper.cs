﻿using Contracts;
using Entities.WeatherModels;
using System.Threading.Tasks;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper 
    { 
        private RepositoryContext _repoContext;
        private IWheatherRepository _weatherRepository;
        private ICityTempratureRepository _cityTempratureRepository;

        public RepositoryWrapper(RepositoryContext repositoryContext) 
        { 
            _repoContext = repositoryContext; 
        }

        
        public IWheatherRepository WheatherRepository
        {
            get
            {
                if (_weatherRepository == null)
                {
                    _weatherRepository = new WheatherRepository();
                }
                return _weatherRepository;
            }
        }

        public ICityTempratureRepository CityTempratureRepository
        {
            get
            {
                if (_cityTempratureRepository == null)
                {
                    _cityTempratureRepository = new CityTempratureRepository(_repoContext);
                }
                return _cityTempratureRepository;
            }
        }
        public async Task SaveAsync() 
        {
            await _repoContext.SaveChangesAsync();
        }

        public void Save()
        {
            _repoContext.SaveChanges();
        }
    }
}
