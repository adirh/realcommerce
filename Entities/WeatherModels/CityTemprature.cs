﻿
namespace Entities.WeatherModels
{
    public partial class CityTemprature
    {
        public long CityCode { get; set; }
        public decimal CelsiusTempreture { get; set; }
        public string WeatherText { get; set; }

        private CityTemprature()
        {

        }
        public CityTemprature(long cityCode, decimal celsiusTempreture, string weatherText)
        {
            CityCode = cityCode;
            CelsiusTempreture = celsiusTempreture;
            WeatherText = weatherText;
        }
    }
}
