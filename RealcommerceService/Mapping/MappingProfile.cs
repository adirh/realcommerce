﻿using AutoMapper;
using Entities.DataTransferObjects;
using Entities.WeatherModels;

namespace AccountOwnerServer
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<APICItyModel, APICItyDTO>();
            CreateMap<CityTemprature, WatherDTO>();
            CreateMap<GetCurrentWeather, WatherDTO>()
                .ForMember(destination => destination.CelsiusTempreture,
               opts => opts.MapFrom(source => source.Temperature.Imperial.Value));
        }
    }
}
