﻿
namespace Entities.DataTransferObjects
{
    public class APICItyDTO
    {
        public string Key { get; set; }
        public string LocalizedName { get; set; }
        public Country Country { get; set; }

        public APICItyDTO()
        {
        }
    }
}
