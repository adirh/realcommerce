﻿using System;

namespace Entities.Exceptions
{
    public class WeatherAPIUnauthorizedException : Exception 
    {
        public WeatherAPIUnauthorizedException():
                base($"Failed to connect to weather api, maybe your token expired.")
        {

        }
    }
}
